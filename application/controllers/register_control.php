<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class register_control extends CI_Controller{
	public function __construct()
  {
	 parent::__construct();
	 $this->load->model('register_model');
  }  

    public function index()
  {
      $this->load->view('register_view');
   
  }

  public function tambah()
	{
		
		if($this->register_model->tambah()) {
			$this->session->set_flashdata('notif', 'Daftar User berhasil');
			redirect('login_control/index');
		} else {
			$this->session->set_flashdata('notif', 'Daftar User gagal');
			redirect('register_control/index');

			}
			
	}

}

