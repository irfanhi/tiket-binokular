<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard_control extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}

	public function index()
  {
 
  	  $data['konten'] = "dashboard_view";
  	  $data['data_tiket'] = $this->dashboard_model->get_data_tiket();
  	  $data['tiket']=$this->dashboard_model->get_tiket();
      $data['repair']=$this->dashboard_model->get_repair();
      $this->load->view('template_view',$data);
  	
  }

  	public function tambah()
  	{

  	if($this->dashboard_model->tambah()) {
		$this->session->set_flashdata('notif', 'Tambah Ticket berhasil');
		redirect('dashboard_control/index');
	} else {
		$this->session->set_flashdata('notif', 'Tambah Ticket gagal');
		redirect('dashboard_control/index');
	}
    }

    public function ticket_history($id_ticket){
    # tampilkan deskripsi tiket
    $data['tiket'] = $this->dashboard_model->get_tiket_detail($id_ticket);
    #echo 'ehlo, ticket_history: ' . $id_ticket;
    #echo '<pre>';
    #print_r($data['tiket']);
    #echo '</pre>';
    
    # tampilkan history repair
    $data['repair'] = $this->dashboard_model->get_repair_detail($id_ticket);
    #echo 'ehlo, repair_history: ' . $id_ticket;
    #echo '<pre>';
    #print_r($data['repair']);
    #echo '</pre>';

    $this->load->view('ticket_history', $data);
    
  }

  public function ticket_history_admin($id_ticket){
    # tampilkan deskripsi tiket
    $data['tiket'] = $this->dashboard_model->get_tiket_detail($id_ticket);
    #echo 'ehlo, ticket_history: ' . $id_ticket;
    #echo '<pre>';
    #print_r($data['tiket']);
    #echo '</pre>';
    
    # tampilkan history repair
    $data['repair'] = $this->dashboard_model->get_repair_detail($id_ticket);
    #echo 'ehlo, repair_history: ' . $id_ticket;
    #echo '<pre>';
    #print_r($data['repair']);
    #echo '</pre>';

    $this->load->view('ticket_history_admin', $data);
    
  }

  public function get_id_tiket(){

    if($this->dashboard_model->get_tiket()) {
          $this->session->set_flashdata('notif', 'Pembayaran berhasil');
          redirect('riwayat/index');
        } else {
          $this->session->set_flashdata('notif', 'Pembayaran gagal');
          redirect('gaji/index');
        }
  }

  
  public function get_tiket_by_id($id)
  {
    if($this->session->userdata('logged_in') == TRUE){

      $data = $this->dashboard_model->get_tiket_by_id($id);
      echo json_encode($data);

    } else {
      redirect('login/index');
    }
  
  }

  public function tambahpro()
  {
    
    if($this->dashboard_model->tambahpro()) {
      $this->session->set_flashdata('notif', 'Add Pprogress berhasil');
      redirect('dashboard_control/index');
    } else {
      $this->session->set_flashdata('notif', 'Add Pprogress gagal');
      redirect('dashboard_control/index');

      }
      
  }
 
}