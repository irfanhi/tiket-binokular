<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_control extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('login_model','login');
  }

  public function index()
  {
    if($this->session->userdata('login') == TRUE){

      redirect('dashboard_control/index');

    } else {
      $this->load->view('login_view');
    }
  }

  public function proses_login()
  {
    if($this->input->post('login')==TRUE){
      $this->form_validation->set_rules('username', 'username', 'trim|required');
      $this->form_validation->set_rules('password', 'password', 'trim|required');
      if ($this->form_validation->run() == TRUE) {
        $this->load->model('login_model','login');
        if ($this->login->get_login()->num_rows()>0) {
          $data=$this->login->get_login()->row();
          $array=array(
            'login'       => TRUE,
            'id_user'     => $data->id_user,
            'username'    => $data->username,
            'password'    => $data->password,
            'nama'        => $data->nama,
            'level'       => $data->level,
            'logged_in'   => TRUE
          );

          $this->session->set_userdata( $array );
          redirect('login_control/index','refresh');
        }else {
        redirect('login_control/index','refresh');
        } 
      }else{
        $this->session->set_flashdata('notif', validation_errors());
        redirect('login_control/index','refresh');
      }
    }
  }

  public function proses_login2()
  {
    if($this->input->post('login')){
      $this->form_validation->set_rules('username', 'username', 'trim|required');
      $this->form_validation->set_rules('password', 'password', 'trim|required');
      if ($this->form_validation->run() == TRUE) {
        $this->load->model('login_model','login');
        if ($this->login->get_login2()->num_rows()>0) {
          $data=$this->login->get_login2()->row();
          $array=array(
            'login'       => TRUE,
            'id_admin'    => $data->id_user,
            'username'    => $data->username,
            'password'    => $data->password,
            'nama'        => $data->nama
          );

          $this->session->set_userdata( $array );
          redirect('login_control/index','refresh');
        }else {
        $this->session->set_flashdata('notif', 'username dan password salah');
        redirect('login_control/index','refresh');
        } 
      }else{
        $this->session->set_flashdata('notif', validation_errors());
        redirect('login_control/index','refresh');
      }
    }
  }


/*
  public function cek_login(){
    if($this->session->userdata('logged_in') == FALSE){

      $this->form_validation->set_rules('username', 'username', 'trim|required');
      $this->form_validation->set_rules('password', 'password', 'trim|required');

      if ($this->form_validation->run() == TRUE) {
        if($this->user->cek_user() == TRUE){
          redirect('login_control/index');
        } else {
          $this->session->set_flashdata('notif', 'username / password salah');
          redirect('login_control/index');
        }
      } else {
        $this->session->set_flashdata('notif', validation_errors());
          redirect('login_control/index');
      }

    } else {
      redirect('login_control/index');
    }
  }
*/

  public function logout(){
    $this->session->sess_destroy();
    redirect('login_control');
  }

  

}

