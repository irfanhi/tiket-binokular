<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tiketlist_control extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('tiketlist_model');
	}

	public function index()
  	{

  	  			$data['konten'] = "tiketlist_view";
  	  			$data['tampil_tiket'] = $this->tiketlist_model->get_data_tiket();
  	  			$data['history'] = $this->tiketlist_model->get_history();
  	  			$data['repair']=$this->tiketlist_model->get_repair();	
      			$this->load->view('template_view',$data);

	}

	public function get_tiket_by_id($id)
  {
    if($this->session->userdata('logged_in') == TRUE){

      $data = $this->tiketlist_model->get_tikett_by_id($id);
      echo json_encode($data);

    } else {
      redirect('login/index');
    }
  }



    
 
}