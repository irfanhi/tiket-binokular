<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

public function tambah()
	{
		$data = array(
		
				'username'			=> $this->input->post('username'),
				'nama'				=> $this->input->post('nama'),
				'password'			=> $this->input->post('password'),
				'level'				=> 'user'
			);

		$this->db->insert('user', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	

}

/* End of file register_model.php */
/* Location: ./application/models/register_model.php */