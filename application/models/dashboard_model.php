<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard_model extends CI_Model {

	public function get_data_tiket()
	{
		return $this->db->get('tiket')
						->result();
	}

	public function tambah()
	{

		$id =$this->session->userdata('id_user');
		$tgl =date('Y-m-d');
		$status =1;
		$data = array(
				'id_user' 		=> $id,
				'tittle'		=> $this->input->post('tittle'),
				'description'	=> $this->input->post('description'),
				'date'			=> $tgl,
				'id_status' 	=> $status

		);

	 	return $this->db->insert('tiket',$data);

	 	if($this->db->affected_rows() > 0) {
			return TRUE;
		} else  {
			return FALSE;
     	}

	}

	public function get_tiket_detail($id_ticket){
		return $this->db->where('id_tiket', $id_ticket)
						->get('tiket')
						->row();
	}

	public function get_repair_detail($id_ticket){
		return $this->db->where('id_tiket', $id_ticket)
						->join('status','status.id_status = repair.id_status')
						->get('repair')
						->result();
	}

	public function get_tiket(){
		return $this->db->join('status','tiket.id_status = status.id_status')
						->join('user','tiket.id_user = user.id_user')
						->get('tiket')
						->result();
	}

	public function get_repair(){
		return $this->db->join('status','repair.id_status = status.id_status')
						->get('repair')
						->result();
	}

	public function get_transaksi_by_id($id)
	{
		return $this->db->select('repair.id_repair, 
								  repair.keterangan,')
						->where('id_repair', $id)
						->join('status','status.id_status = repair.id_status')
						->get('repair')
						->result();
	}

	public function get_tiket_by_id($id)
	{
		return $this->db->where('id_tiket', $id)
						->get('tiket')
						->row();
	}
		public function tambahpro()
	{	
		$tgl =date('Y-m-d');
		$data = array(
				'tanggal'				=> $tgl,
				'id_user'              	=>  $this->session->userdata('id_user'),
				'id_status' 			=> $this->input->post('id_status'),
				'keterangan'			=> $this->input->post('keterangan'),
				'id_tiket'				=> $this->input->post('id_tiket'),
			);

		$this->db->insert('repair', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}	

	
}