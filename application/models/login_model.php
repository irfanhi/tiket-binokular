<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_model extends CI_Model {

/*
    public function cek_user(){
		$u = $this->input->post('username');
		$p = $this->input->post('password');

		$query = $this->db->where('username', $u)
						  ->where('password', $p)
						  ->get('user');
		if($this->db->affected_rows() > 0){

			$data_login = $query->row();

			$data_session = array(
									'username'  => $data_login->username,
									'logged_in' => TRUE,
								);
			$this->session->set_userdata($data_session);

			return TRUE;
		} else {
			return FALSE;
		}
	}
*/

	public function get_login()
	{
		return $this->db
			->where('username',$this->input->post('username'))
			->where('password',$this->input->post('password'))
			->get('user'); 
	}

	public function get_login2()
	{
		return $this->db
			->where('username',$this->input->post('username'))
			->where('password',$this->input->post('password'))
			->get('admin'); 
	}

}