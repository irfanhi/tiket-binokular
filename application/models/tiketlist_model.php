<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tiketlist_model extends CI_Model {

	public function get_data_tiket()
	{
		return $this->db->join('status','status.id_status=tiket.id_status')
						->where($this->session->userdata('id_user'))
						->get('tiket')
						->result();
	}

	public function get_tiket_by_id($id)
	{
		return $this->db->select('tiket.id_tiket, tiket.id_status')
						->where('id_tiket', $id)
						->join('tiket','tiket.id_tiket = repair.id_tiket')
						->get('repair')
						->result();
	}
	public function get_repair(){
		return $this->db->join('status','repair.id_status = status.id_status')

						->get('repair')
						->result();
	}

	public function get_history() {

		$query = 'SELECT * 
				    FROM tiket 
				    JOIN user 
				      ON user.id_user = tiket.id_user 
				    JOIN status 
				      ON status.id_status = tiket.id_status 
			       WHERE user.id_user = '.$this->session->userdata('id_user').'
		';

		$rekap_absensi = $this->db->query($query)->result();
		return $rekap_absensi;
		

	}
	public function get_tikett_by_id($id)
	{
		return $this->db->where('id_tiket', $id)
						->get('tiket')
						->row();
	}

	
}