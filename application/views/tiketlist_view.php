 <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">tiket List</h4>
                                <p class="category">List tiket anda</p>
                                            <?php
                $notif = $this->session->flashdata('notif');
                if($notif != NULL){
                    echo '<div class="alert alert-danger">'.$notif.'</div>';
                }
            ?>

                                  </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>No</th>
                                        <th>tiket ID</th>
                                        <th>STATUS</th>
                                        <th>HISTORY</th>
                                    </thead>
                                    <tbody>

                            <?php 
                            $no = 1;
                            foreach ($history as $ses) {
                                echo '
                                <tr>
                                <td>'.$no.'</td>
                                <td>'.$ses->id_tiket.'</td>
                                <td>'.$ses->nama_status.'</td>
                                <td>
                                    <a href="http://localhost/tiket/index.php/dashboard_control/ticket_history/'.$ses->id_tiket.'" class="btn btn-info btn-sm" target="_blank">History </a>
                                </td>
                                </tr> 
                                        ';
                                    $no++;
                                }
                            ?>                               
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



<script type="text/javascript">

    function prepare_detil_tiket(id)
    {
        $(".modal-body").empty();
        $.getJSON('<?php echo base_url(); ?>index.php/tiketlist_control/tiket_id/' + id,  function(data){
            $(".modal-body").html(data.show_detil);
        });

    }
</script>

