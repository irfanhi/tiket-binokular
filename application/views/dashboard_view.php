         <?php if ($this->session->userdata('level')=="user"): ?>
          <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <form class="form-horizontal"  action="<?php echo base_url('index.php/dashboard_control/tambah'); ?>" method="post"> 
                    <div class="col-md-12">
                      <?php if ($this->session->flashdata('notif')): ?>
                          <div class="alert alert-info"><?= $this->session->flashdata('notif');?></div>
                      <?php endif ?>
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Create Ticket</h4>
                                <p class="category">Buat ticket anda</p>
                            </div>
                            <div class="content">
                               <div class="footer">
                                <table class="table">
                                                
                            <tr>
                              <td>Title :</td>
                              <td>
                                <div class="form-group">
                                  <input type="text" name="tittle" class="form-control" style="width: 800px;">
                                </div>
                              </td>
                            </tr>
                              
                            <tr>
                              <td>Description :</td>
                              <td>
                                <div class="form-group">
                                  <textarea class="form-control" name="description" rows="5" style="width: 800px;"></textarea>
                                </div>
                              </td>
                            </tr>

                            <tr>
                              <td></td>
                              <td>
                                <button name="submit" type="submit" class="btn btn-info btn-fill pull-right" style="margin-right: 65px;">Submit ticket</button>
                              </td>
                            </tr>
                        </table>

                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endif ?>

 <?php if ($this->session->userdata('level')=="admin"): ?>
         <div class="content">
                            <?php
                                $notif = $this->session->flashdata('notif');
                                if($notif != NULL){
                                    echo '
                                        <div class="alert alert-danger">'.$notif.'</div>
                                    ';
                                }
                            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">List Ticket Client</h4>
                                <p class="category">List ticket client anda</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>NO</th>
                                        <th>TICKET ID</th>
                                        <th>NAMA</th>
                                        <th>STATUS</th>
                                        <th>HISTORY</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                          $no = 1;
                                          foreach ($tiket as $t) {
                                          echo '
                                            <tr>
                                                <td>'.$no.'</td>
                                                <td>'.$t->id_tiket.'</td>
                                                <td>'.$t->username.'</td>
                                                <td>'.$t->nama_status.'</td>
                                                
                                                <td>
                                                    
                                                    <a href="http://localhost/tiket/index.php/dashboard_control/ticket_history_admin/'.$t->id_tiket.'" target="_blank" class="btn btn-info btn-sm">History</a>

                                                    <a href="#edit" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_progress_tiket" onclick="prepare_show_history('.$t->id_tiket.')">Add Progress</a>
                                                </td>

                                            </tr>
                                                  ';
                                          $no++;
                                         }
                                        ?>
                                </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                   


                </div>
            </div>
        </div>

<?php endif ?>