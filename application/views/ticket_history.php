<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="<?=base_url()?>assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Binokular tiket</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?=base_url()?>BS3/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?=base_url()?>BS3/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?=base_url()?>BS3/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?=base_url()?>BS3/assets/css/demo.css" rel="stylesheet" />


    <link href="<?=base_url()?>BS3/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="black" data-image="<?=base_url()?>assets/img/sidebar-2.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="<?=base_url('index.php/dashboard_control/index')?>" class="simple-text">
                    Binokular tiket
                </a>
            </div>

            <ul class="nav">
              
                <li >
                    <a href="<?=base_url('index.php/dashboard_control/index')?>">
                        <i class="pe-7s-note"></i>
                        <p>New tiket</p>
                    </a>
                </li>
                <li >
                    <a href="<?=base_url('index.php/tiketlist_control/index')?>">
                        <i class="pe-7s-note2"></i>
                        <p>tiket List</p>
                    </a>
                </li>
            </ul>
           
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret hidden-lg hidden-md"></b>
									<p class="hidden-lg hidden-md">
										5 Notifications
										<b class="caret"></b>
									</p>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
                        <li>
                           <a href="">
                                <i class="fa fa-search"></i>
								<p class="hidden-lg hidden-md">Search</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                   

                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li class="divider"></li>
                                <li><a href="<?=base_url('index.php/login_control/logout')?>">
                                <i class="zmdi zmdi-power"></i>Logout</a></li>
                              </ul>
                        </li>
						<li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


								<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">History tiket</h4>
                                <p class="category">Riwayat tiket anda</p>
                            </div>
                           
                            <div class="col-md-12">
                                <div class="header">
                                    <table>
                                        <tr>
                                          <td>Description :</td>
                                          <td>
                                            
                                              <h5 style="margin-left: 10px;"><?php echo $tiket->description; ?></h5>
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>Tiket masuk pada tanggal :</td>
                                          <td>
                                              <h5 style="margin-left: 10px;">
                                                
                                                <?php $tanggal = date_create($tiket->date);
                                                $tanggal = date_format($tanggal, 'd F , Y '); 
                                                echo $tanggal;

                                                ?></h5>
                                          </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>NO</th>
                                        <th>TANGGAL</th>
                                        <th>STATUS</th>
                                        <th>Keterangan</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                          $no = 1;
                                          foreach ($repair as $t) {
                                          $date = date_create($t->tanggal);
                                          $date = date_format($date, 'd F , Y ');
                                          echo '
                                            <tr>
                                                <td>'.$no.'</td>
                                                <td>'.$date.'</td>
                                                <td>'.$t->nama_status.'</td>
                                                <td>'.$t->keterangan.'</td>
                                      
                                            </tr>
                                                  ';
                                          $no++;
                                         }
                                        ?>
                                </tbody>
                                </table>

                                
                            </div>
                        </div>
                    </div>

    
                </div>
            </div>



    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?=base_url()?>BS3/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>BS3/assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Notifications Plugin    -->
    <script src="<?=base_url()?>BS3/assets/js/bootstrap-notify.js"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="<?=base_url()?>BS3/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="<?=base_url()?>BS3/assets/js/demo.js"></script>


</html>
        <div id="modal_progress_tiket" class="modal fade" role="dialog">
            <div class="modal-dialog">
    <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title">Add Progress</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      
                  </div>
                 <form action="<?php echo base_url('index.php/historytiket_control/tambah'); ?>" method="post">
                   <div class="modal-body">
                      <select class="form-control" name="id_status" style="width:300px;">
                            
                                <option value="2">In Progress</option>
                                <option value="3">Done</option>    
            
                      </select>
                      <br>
                      <textarea type=text placeholder="Keterangan" name="keterangan" rows="5" class="form-control" id="keterangan" style="width:500px;"></textarea>
                      <br>
                   </div>
                  
                  <div class="modal-footer">
                     <input type="submit" class="btn btn-primary" name="submit" value="Add">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                 </form>
              </div>
            </div>
        </div>
