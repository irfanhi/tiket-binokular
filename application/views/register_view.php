<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Binokular Ticket</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?=base_url()?>BS3/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?=base_url()?>BS3/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?=base_url()?>BS3/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?=base_url()?>BS3/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="<?=base_url()?>http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?=base_url()?>BS3/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>


<div class="col-md-5" style="margin-left: 25%; margin-top: 7%;">

                       <form action="<?php echo base_url('index.php/register_control/tambah'); ?>" method="post"> 
                        <div class="card">
                           <?php
                              $notif = $this->session->flashdata('notif');
                              if($notif != NULL){
                              echo '<div class="alert alert-danger">'.$notif.'</div>';
                            }
                          ?>
                            <center>
                            <div class="header">
                                <h4 class="title">Register</h4>
                                <p class="category"></p>
                            </div>
                            </center>
                            <div class="content">
                               <div class="footer">
                                <table class="table">

                             <tr>
                              <td>Nama :</td>
                              <td>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="nama" style="width: 350px;">
                                </div>
                              </td>
                            </tr>

                            <tr>
                              <td>Username :</td>
                              <td>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="username" style="width: 350px;">
                                </div>
                              </td>
                            </tr>

                            <tr>
                              <td>Password :</td>
                              <td>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="password" style="width: 350px;">
                                </div>
                              </td>
                            </tr>

                            <tr>
                              <td></td>
                              <td>
                                <button type="submit" class="btn btn-info btn-fill pull-right" style="margin-right: 150px; width: 150px;">Register</button>

                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td>
                            <label style="margin-left: 55px;">Already have an account? <a href="<?=base_url('index.php/login_control/index')?>">Login</a></label>
                            </td>
                            </tr>
                        </table>
                                    
                                </div>
                            </div>
                        </div>
                       </form> 
                    </div>

    <!--   Core JS Files   -->
    <script src="<?=base_url()?>BS3/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>BS3/assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Charts Plugin -->
    <script src="<?=base_url()?>BS3/assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?=base_url()?>BS3/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="<?=base_url()?>BS3/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="<?=base_url()?>BS3/assets/js/demo.js"></script>

</html>
